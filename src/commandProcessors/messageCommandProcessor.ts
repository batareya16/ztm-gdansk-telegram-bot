import BotWrapper from "../infrastructure/BotWrapper";
import CommandProcessor from "../infrastructure/commands/commandProcessor";
import {MessageCommandsRegistry} from "../infrastructure/commands/registry";
import CommandCallback from "../infrastructure/commands/commandCallback";

export class MessageCommandProcessor implements CommandProcessor {
  public async processCommand(bot: BotWrapper, request: BotRequest): Promise<void> {
    for (const cmdType of MessageCommandsRegistry.getCommands()) {
      const cmd = new cmdType();
      if (request.text != null && cmd.validateInput(request.text)) {
        await cmd.processMessage(bot, request.text);
      }

      if (request.data === undefined) {
        continue;
      }

      const callback: CommandCallback = JSON.parse(request.data);
      if (cmd.getCallBackName() == callback.name) {
        await cmd.processCallback(bot, callback);
      }
    }
  }

  public getProcessorType(): string {
    return "text";
  }
}
