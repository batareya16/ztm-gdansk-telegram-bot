import BotWrapper from "../infrastructure/BotWrapper";
import CommandProcessor from "../infrastructure/commands/commandProcessor";
import NearestStopCommand from "../commands/ztm_gdansk/nearestStop/nearestStopCommand";

export class LocationCommandProcessor implements CommandProcessor {
  public async processCommand(bot: BotWrapper, request: BotRequest): Promise<void> {
    switch (process.env.ENVIRONMENT as string) {
      case "ZTM_GDANSK":
        if (request.location !== undefined) {
          await new NearestStopCommand().processLocation(bot, request.location.latitude, request.location.longitude);
          return;
        }
        break;
      default:
        await bot.sendTextMessage("👷🏻We don't support location command for this bot yet. Please use /help to see available commands.")
        break;
    }
  }

  public getProcessorType(): string {
    return "location";
  }
}
