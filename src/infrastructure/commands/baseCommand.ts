export default interface BaseCommand {
  getCallBackName(): string;
}
