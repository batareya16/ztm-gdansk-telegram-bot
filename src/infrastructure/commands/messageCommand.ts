import BotWrapper from "../BotWrapper";
import BaseCommand from "./baseCommand";
import CommandCallback from "./commandCallback";

export default interface MessageCommand extends BaseCommand {
  processMessage(bot: BotWrapper, input: string): Promise<void>;
  processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void>;
  validateInput(input: string): boolean;
}
