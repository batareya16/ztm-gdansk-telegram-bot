import BotWrapper from "../BotWrapper";

export default interface CommandProcessor {
  processCommand(bot: BotWrapper, request: BotRequest): Promise<void>;
  getProcessorType(): string;
}
