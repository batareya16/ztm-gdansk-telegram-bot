import {LocationCommandProcessor} from "../../commandProcessors/locationCommandProcessor";
import {MessageCommandProcessor} from "../../commandProcessors/messageCommandProcessor";
import AdminCommand from "../../commands/common/admin/adminCommand";
import StartCommand from "../../commands/common/start/startCommand";
import HelpCommand from "../../commands/common/help/helpCommand";

import ZTMGdansk_RouteInfoCommand from "../../commands/ztm_gdansk/routeInfo/routeInfoCommand";
import ZTMGdansk_StopInfoCommand from "../../commands/ztm_gdansk/stopInfo/stopInfoCommand";
import ZTMGdansk_TicketInfoCommand from "../../commands/ztm_gdansk/ticketInfo/ticketInfoCommand";

import SKMTrojmiasto_StopInfoCommand from "../../commands/skm_trojmiasto/stopInfo/stopInfoCommand";
import AllBotsCommand from "../../commands/common/allBots/allBotsCommand";

export class CommandProcessorsRegistry {
  public static getProcessors() {
    return [MessageCommandProcessor, LocationCommandProcessor];
  }
}

export class MessageCommandsRegistry {
  public static getCommands() {
    switch (process.env.ENVIRONMENT as string) {
      case "SKM_TROJMIASTO": return [SKMTrojmiasto_StopInfoCommand, AdminCommand, StartCommand, HelpCommand, AllBotsCommand];
      case "ZTM_GDANSK": return [ZTMGdansk_StopInfoCommand, ZTMGdansk_RouteInfoCommand, AdminCommand, StartCommand, HelpCommand, ZTMGdansk_TicketInfoCommand, AllBotsCommand];
    }

    return [];
  }
}
