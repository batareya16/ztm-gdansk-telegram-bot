import TeleBot from 'telebot'
import InlineKeyboard from "./telebot/InlineKeyboard";
import {InlineKeyboardType} from "./telebot/InlineKeyboardType";

export default class BotWrapper {
  private telebot: TeleBot;
  private readonly userId: number;
  private readonly chatId: number | undefined;
  private readonly messageId: number | undefined;
  private readonly localeId: string;

  constructor(telebot: TeleBot, bot: BotRequest) {
    this.telebot = telebot;
    this.userId = bot.from.id;
    this.chatId = bot.message?.chat.id ?? bot.chat.id;
    this.messageId = bot.message?.message_id;
    this.localeId = bot.from.language_code;
  }

  public getLanguageCode(): string {
    return this.localeId;
  }

  public async sendTextMessage(message: string): Promise<void> {
    await this.telebot.sendMessage(this.chatId ?? this.userId, BotWrapper.prepareMessage(message), { parseMode: "MarkdownV2" });
  }

  public async sendTextWithKeyboard(message: string, keyboard: InlineKeyboard | null): Promise<void> {
    await this.telebot.sendMessage(this.chatId ?? this.userId, BotWrapper.prepareMessage(message), {
      replyMarkup: keyboard === null ? null : this.constructKeyboard(keyboard),
      parseMode: "MarkdownV2" });
  }

  public async editMessageTextWithKeyboard(message: string, keyboard: InlineKeyboard): Promise<void> {
    if (this.chatId === undefined || this.messageId === undefined) {
      return;
    }

    const opts: any = { replyMarkup: this.constructKeyboard(keyboard), parseMode: "MarkdownV2" }
    await this.telebot.editMessageText(
      {chatId: this.chatId, messageId: this.messageId},
      BotWrapper.prepareMessage(message),
      opts);
  }

  public async sendFile(filePath: string, displayFileName: string): Promise<void> {
    if (this.chatId === undefined) {
      return;
    }

    await this.telebot.sendDocument(this.chatId, filePath, { fileName: displayFileName });
  }

  public async editMessageText(message: string): Promise<void> {
    if (this.chatId === undefined || this.messageId === undefined) {
      return;
    }

    await this.telebot.editMessageText(
      {chatId: this.chatId, messageId: this.messageId},
      BotWrapper.prepareMessage(message),
      { parseMode: "MarkdownV2" });
  }

  private static prepareMessage(text: string) {
    if (text.length > 4096) {
      text = text.substring(0, 4088).concat("``` ...");
    }

    return text
      .replaceAll(".", "\\.")
      .replaceAll("-", "\\-")
      .replaceAll("+", "\\+")
      .replaceAll("(", "\\(")
      .replaceAll(")", "\\)")
      .replaceAll("!", "\\!");
  }

  private constructKeyboard(keyboard: InlineKeyboard) {
    if (keyboard == null || keyboard.buttons.length == 0) {
      return null;
    }

    const resultButtons: any[] = [];
    keyboard.buttons.forEach((btn, i) => {
      const columnsCount = keyboard.type == InlineKeyboardType.Horizontal ? 5 : 1;
      if (i % columnsCount == 0) {
        resultButtons.push([]);
      }

      resultButtons[resultButtons.length - 1].push(this.telebot.inlineButton(btn.text, {callback: btn.callback }))
    });

    return this.telebot.inlineKeyboard(resultButtons);
  }
}
