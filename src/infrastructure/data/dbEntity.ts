interface DbEntity {
  getPrimaryKey(): string;
  getSelectColumns(): string;
}
