import InlineKeyboardButton from "./InlineKeyboardButton";
import {InlineKeyboardType} from "./InlineKeyboardType";

export default class InlineKeyboard {
  private readonly _buttons: InlineKeyboardButton[];
  private readonly _type: InlineKeyboardType;

  get buttons(): InlineKeyboardButton[] {
    return this._buttons;
  }

  get type(): InlineKeyboardType {
    return this._type;
  }

  constructor(buttons: InlineKeyboardButton[], type: InlineKeyboardType) {
    this._buttons = buttons;
    this._type = type;
  }
}
