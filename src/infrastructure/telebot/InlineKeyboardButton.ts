export default class InlineKeyboardButton {
  private readonly _text: string;
  private readonly _callback: string;

  public get callback(): string {
    return this._callback;
  }
  public get text(): string {
    return this._text;
  }

  constructor(text: string, callback: string) {
    this._text = text;
    this._callback = callback;
  }
}
