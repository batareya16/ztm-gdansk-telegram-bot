class BotRequest {
  public text!: string;
  public data!: string;
  public callback!: string;
  public from!: { id: number, first_name: string, last_name: string, username: string, language_code: string };
  public chat!: { id: number };
  public date!: number;
  public message!: { message_id: number, chat: { id: number } } | undefined;
  public location!: { latitude: number, longitude: number } | undefined;
}
