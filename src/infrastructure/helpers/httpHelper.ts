import axios, {AxiosResponse} from "axios";

export async function httpGet<T>(url: string): Promise<T> {
  return axios.get(url).then((x: AxiosResponse) => x.data)
}
