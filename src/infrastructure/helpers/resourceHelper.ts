import resources from "../../../resources.json";

export function getTranslation(language: string, key: string, isCommon?: boolean): string {
  // @ts-ignore
  return isCommon === true ? resources["COMMON"][language][key] : resources[process.env.ENVIRONMENT as string][language][key];
}
