import UserActionRepository from "../../data/common/repositories/userActionRepository";
import {getUnixTime} from "../../commands/infrastructure/dateHelper";

export default class UserActionHelper {
  private userActionsRepository: UserActionRepository = new UserActionRepository();

  public async validateUserAction(request: BotRequest) {
    const lastAction: UserAction = (await this.userActionsRepository.getLastUserAction(request.from.id)).rows[0];
    const currTimestamp = request.date || getUnixTime(new Date());

    if (lastAction !== undefined && lastAction.Timestamp == currTimestamp) {
      throw new Error(`User ${lastAction.UserName} ${lastAction.FirstName} ${lastAction.LastName} is sending messages too fast..`);
    }

    const newAction: UserAction = {
      UserId: request.from.id,
      Timestamp: currTimestamp,
      ChatId: request.chat?.id,
      FirstName: request.from.first_name,
      LastName: request.from.last_name,
      UserName: request.from.username,
      LangCode: request.from.language_code,
      Message: request.text,
      Callback: request.data
    }

    await this.userActionsRepository.insertUserAction(newAction);
  }
}
