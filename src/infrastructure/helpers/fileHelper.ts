import tmp, {FileResult} from 'tmp';
import fs from 'fs';

export function generateTempCsvFile(rows: Array<any>): FileResult {
  let csvContent = "";

  // Loop the array of objects
  for (let row = 0; row < rows.length; row++) {
    let keysAmount = Object.keys(rows[row]).length
    let keysCounter = 0

    // If this is the first row, generate the headings
    if (row === 0) {

      // Loop each property of the object
      for (let key in rows[row]) {

        // This is to not add a comma at the last cell
        // The '\r\n' adds a new line
        csvContent += key + (keysCounter + 1 < keysAmount ? ',' : '\r\n' )
        keysCounter++
      }
    } else {
      for (let key in rows[row]){
        csvContent += rows[row][key] + (keysCounter + 1 < keysAmount ? ',' : '\r\n' )
        keysCounter++
      }
    }

    keysCounter = 0
  }

  const tmpObj: FileResult = tmp.fileSync();
  fs.writeSync(tmpObj.fd, csvContent);
  return tmpObj;
}

export function readTextFromFile(filePath: string): string {
  return fs.readFileSync(filePath, { encoding: 'utf8' });
}
