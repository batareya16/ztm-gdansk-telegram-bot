export function addDays(date: Date, days: number): Date {
  const result = new Date(date.valueOf());
  result.setDate(date.getDate() + days);
  return result;
}

export function formatDate(date: Date): string {
  return `${date.getDate()}.${(date.getMonth()+1)}.${date.getFullYear()}`;
}

export function getToday(): Date {
  const result = new Date();
  result.setHours(0, 0, 0, 0);
  return result;
}

export function getUnixTime(date: Date) {
  return Math.floor(date.getTime() / 1000)
}
