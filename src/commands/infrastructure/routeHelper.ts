export function getTypeSymbol(type: string): string {
  switch (type) {
    case "BUS": return "🚌"
    case "TRAM": return "🚋"
  }

  return "";
}

export function reverseStops(route: string) {
  return route.split('-').map((x: string) => x.trim()).reverse().join(" - ");
}

export function getEndStation(route: string) {
  return route.split('-')[1].trim();
}

