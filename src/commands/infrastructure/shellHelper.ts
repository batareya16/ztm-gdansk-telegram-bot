import * as child from 'child_process';

export function executeShellScript(shellCmd: string) {
  child.exec(shellCmd);
}

export function executeSyncShellScript(shellCmd: string) {
  child.execSync(shellCmd);
}
