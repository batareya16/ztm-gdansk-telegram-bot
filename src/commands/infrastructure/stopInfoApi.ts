import {httpGet} from "../../infrastructure/helpers/httpHelper";

export class Api {
  static async getStopInfo(stopId: number) : Promise<StopInfoResponse> {
    return await httpGet<StopInfoResponse>(`https://ckan2.multimediagdansk.pl/delays?stopId=${stopId}`);
  }
}

export class StopInfoResponse {
  public lastUpdate!: Date;
  public delay!: Delay[];
}

class Delay {
  public id!: string;
  public delayInSeconds!: number;
  public estimatedTime!: string;
  public headsign!: string;
  public routeId!: number;
  public tripId!: number;
  public status!: string;
  public theoreticalTime!: string;
  public timestamp!: string;
  public trip!: number;
  public vehicleCode!: number;
  public vehicleId!: number;
}
