import InlineKeyboard from "../../infrastructure/telebot/InlineKeyboard";

export default interface CommandTemplate {
  constructMessage(): string;
  constructKeyboard(callbackName: string) : Promise<InlineKeyboard | null>;
}
