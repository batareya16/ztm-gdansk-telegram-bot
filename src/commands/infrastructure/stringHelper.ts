export default class StringHelper {
  public static initialize() {
    String.prototype.replaceAll = function(search: string, replacement: any) {
      var target = this;
      return target.split(search).join(replacement);
    };
  }
}
