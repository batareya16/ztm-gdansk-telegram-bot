import MessageCommand from "../../../infrastructure/commands/messageCommand";
import BotWrapper from "../../../infrastructure/BotWrapper";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import StartTemplateFactory from "./startTemplateFactory";
import StartCallbackData from "./startCallbackData";

export default class StartCommand implements MessageCommand {
  getCallBackName(): string {
    return "sa";
  }

  async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    const callbackData: StartCallbackData = callback.data;
    const template = StartTemplateFactory.constructTemplate(callbackData.loc);
    await bot.editMessageTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const template = StartTemplateFactory.constructTemplate(bot.getLanguageCode());
    await bot.sendTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  validateInput(input: string): boolean {
    return input === "/start";
  }
}
