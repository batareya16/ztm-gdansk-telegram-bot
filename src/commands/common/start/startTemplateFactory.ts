import StartTemplate from "./startTemplate";

export default class StartTemplateFactory {
  public static constructTemplate(userLocale: string): StartTemplate {
    return new StartTemplate(userLocale);
  }
}
