import HelpTemplate from "./helpTemplate";

export default class HelpTemplateFactory {
  public static constructTemplate(userLocale: string): HelpTemplate {
    return new HelpTemplate(userLocale);
  }
}
