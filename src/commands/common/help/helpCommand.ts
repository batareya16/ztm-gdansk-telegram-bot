import MessageCommand from "../../../infrastructure/commands/messageCommand";
import BotWrapper from "../../../infrastructure/BotWrapper";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import HelpTemplateFactory from "./helpTemplateFactory";
import HelpCallbackData from "./helpCallbackData";

export default class HelpCommand implements MessageCommand {
  getCallBackName(): string {
    return "he";
  }

  async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    const callbackData: HelpCallbackData = callback.data;
    const template = HelpTemplateFactory.constructTemplate(callbackData.loc);
    await bot.editMessageTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const template = HelpTemplateFactory.constructTemplate(bot.getLanguageCode());
    await bot.sendTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  validateInput(input: string): boolean {
    return input === "/help";
  }
}
