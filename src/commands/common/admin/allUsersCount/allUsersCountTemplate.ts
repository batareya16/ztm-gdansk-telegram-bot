import CommandTemplate from "../../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {constructStatsKeyboard} from "../adminTemplateHelper";
import {AdminCommandType} from "../adminCommandType";

export default class AllUsersCountTemplate implements CommandTemplate{
  usersCount: number

  constructor(usersCount: number) {
    this.usersCount = usersCount;
  }

  constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    return constructStatsKeyboard(callbackName, AdminCommandType.AllUsersCount);
  }

  constructMessage(): string {
    return `Total unique users count is: ${this.usersCount}`;
  }
}
