import AllUsersCountTemplate from "./allUsersCountTemplate";

export default class AllUsersCountTemplateFactory {
  public static async constructTemplate(usersCount: number): Promise<AllUsersCountTemplate> {
    return new AllUsersCountTemplate(usersCount);
  }
}
