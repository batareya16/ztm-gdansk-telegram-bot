import CommandTemplate from "../../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import {AdminCommandType} from "../adminCommandType";

export default class CommandsListTemplate implements CommandTemplate {

  constructKeyboard(callbackName: string): Promise<InlineKeyboard | null> {
    const callback = new CommandCallback();
    callback.name = callbackName;

    return Promise.resolve(new InlineKeyboard([
      new InlineKeyboardButton("All users count", JSON.stringify({...callback, data: {type: AdminCommandType.AllUsersCount}})),
      new InlineKeyboardButton("Last week requests", JSON.stringify({...callback, data: {type: AdminCommandType.LastWeekRequests}})),
      new InlineKeyboardButton("Last week actions report", JSON.stringify({...callback, data: {type: AdminCommandType.LastWeekReport}})),
      new InlineKeyboardButton("Worker logs", JSON.stringify({...callback, data: {type: AdminCommandType.WorkerLogs}})),
      new InlineKeyboardButton("Bot logs", JSON.stringify({...callback, data: {type: AdminCommandType.BotLogs}})),
      new InlineKeyboardButton("Restart worker", JSON.stringify({...callback, data: {type: AdminCommandType.TriggerWorker}}))
    ], InlineKeyboardType.Vertical));
  }

  constructMessage(): string {
    return "Select command:";
  }
}
