import CommandsListTemplate from "./commandsListTemplate";

export default class CommandsListTemplateFactory {
  public static async constructTemplate(): Promise<CommandsListTemplate> {
    return new CommandsListTemplate();
  }
}
