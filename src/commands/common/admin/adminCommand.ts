import MessageCommand from "../../../infrastructure/commands/messageCommand";
import BotWrapper from "../../../infrastructure/BotWrapper";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import CommandsListTemplateFactory from "./commandsList/commandsListTemplateFactory";
import {AdminCommandType} from "./adminCommandType";
import CommandTemplate from "../../infrastructure/commandTemplate";
import AllUsersCountTemplateFactory from "./allUsersCount/allUsersCountTemplateFactory";
import UserActionRepository from "../../../data/common/repositories/userActionRepository";
import LastWeekRequestsTemplateFactory from "./lastWeekRequests/lastWeekRequestsTemplateFactory";
import UserRequests from "./lastWeekRequests/UserRequests";
import LastWeekReportTemplateFactory from "./lastWeekReport/lastWeekReportTemplateFactory";
import LastWeekReportTemplate from "./lastWeekReport/lastWeekReportTemplate";
import {generateTempCsvFile} from "../../../infrastructure/helpers/fileHelper";
import BotLogsTemplateFactory from "./botLogs/botLogsTemplateFactory";
import WorkerLogsTemplateFactory from "./workerLogs/workerLogsTemplateFactory";
import {executeShellScript, executeSyncShellScript } from "../../infrastructure/shellHelper";
import TriggerWorkerTemplateFactory from "./triggerWorker/triggerWorkerTemplateFactory";

export default class AdminCommand implements MessageCommand {
  private userActionsRepository: UserActionRepository = new UserActionRepository();

  getCallBackName(): string {
    return "ad";
  }

  async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    let template: CommandTemplate | undefined;
    switch (callback.data.type) {
      case AdminCommandType.CommandsList: template = await this.getCommandsListTemplate(); break;
      case AdminCommandType.AllUsersCount: template = await this.getUsersCountTemplate(); break;
      case AdminCommandType.LastWeekRequests: template = await this.getLastWeekRequestsTemplate(); break;
      case AdminCommandType.TriggerWorker: template = await this.getTriggerWorkerTemplate(bot); break;
      case AdminCommandType.BotLogs: template = await BotLogsTemplateFactory.constructTemplate(); break;
      case AdminCommandType.WorkerLogs: template = await WorkerLogsTemplateFactory.constructTemplate(); break;

      case AdminCommandType.LastWeekReport: await this.processLastWeekReport(bot); return;
    }

    if (template !== undefined) {
      let keyboard = await template!.constructKeyboard(this.getCallBackName());
      if (keyboard == null) { await bot.editMessageText(template.constructMessage()); }
      else { await bot.editMessageTextWithKeyboard(template.constructMessage(), keyboard); }
    }
  }

  async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const template = await this.getCommandsListTemplate();
    await bot.sendTextWithKeyboard(
      template.constructMessage(),
      await template!.constructKeyboard(this.getCallBackName()));
  }

  validateInput(input: string): boolean {
    return input === "/admin";
  }

  async getCommandsListTemplate(): Promise<CommandTemplate> {
    return await CommandsListTemplateFactory.constructTemplate();
  }

  async getUsersCountTemplate(): Promise<CommandTemplate> {
    const usersCount = await this.userActionsRepository.getUniqueUsersCount();
    return await AllUsersCountTemplateFactory.constructTemplate(usersCount.rows[0].count);
  }

  async getLastWeekRequestsTemplate(): Promise<CommandTemplate> {
    const usersRequests: UserRequests[] = (await this.userActionsRepository.getLastWeekUsersRequests()).rows;
    return await LastWeekRequestsTemplateFactory.constructTemplate(usersRequests);
  }

  async getTriggerWorkerTemplate(bot: BotWrapper): Promise<CommandTemplate> {
    try {
      executeSyncShellScript('sudo killall dotnet');
    } catch (e) { }
    executeShellScript('sudo /home/pi/Desktop/workerv2.sh > /home/pi/workerlogs.txt');
    return TriggerWorkerTemplateFactory.constructTemplate();
  }

  async processLastWeekReport(bot: BotWrapper): Promise<void> {
    const usersActions: UserAction[] = (await this.userActionsRepository.getLastWeekUserActions()).rows;
    const template: LastWeekReportTemplate = await LastWeekReportTemplateFactory.constructTemplate();
    try {
      await bot.editMessageTextWithKeyboard(
        template.constructMessage(),
        await template.constructKeyboard(this.getCallBackName()));
    }
    catch (e) { /* for refresh button working */ }
    const reportFile = generateTempCsvFile(usersActions);
    await bot.sendFile(reportFile.name, reportFile.name + ".csv");
  }
}
