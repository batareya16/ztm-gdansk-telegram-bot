import TriggerWorkerTemplate from "./triggerWorkerTemplate";

export default class TriggerWorkerTemplateFactory {
  public static async constructTemplate(): Promise<TriggerWorkerTemplate> {
    return new TriggerWorkerTemplate();
  }
}
