import CommandTemplate from "src/commands/infrastructure/commandTemplate";

export default class TriggerWorkerTemplate implements CommandTemplate {
    constructMessage(): string { return "Worker has been restarted." }
    constructKeyboard(callbackName: string): Promise<null> { return Promise.resolve(null); }
}
