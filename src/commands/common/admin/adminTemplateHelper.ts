import {AdminCommandType} from "./adminCommandType";
import InlineKeyboard from "../../../infrastructure/telebot/InlineKeyboard";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import {AdminCallbackData} from "./adminCallbackData";
import InlineKeyboardButton from "../../../infrastructure/telebot/InlineKeyboardButton";
import {InlineKeyboardType} from "../../../infrastructure/telebot/InlineKeyboardType";

export function constructStatsKeyboard(callbackName: string, cmdType: AdminCommandType): Promise<InlineKeyboard> {
  const callback = new CommandCallback();
  callback.name = callbackName;
  const listCallbackData = new AdminCallbackData();
  listCallbackData.type = AdminCommandType.CommandsList;
  const currCmdCallbackData = new AdminCallbackData();
  currCmdCallbackData.type = cmdType;
  return Promise.resolve(new InlineKeyboard([
    new InlineKeyboardButton("🔄", JSON.stringify({...callback, data: currCmdCallbackData})),
    new InlineKeyboardButton("🔙", JSON.stringify({...callback, data: listCallbackData}))
  ], InlineKeyboardType.Horizontal));
}
