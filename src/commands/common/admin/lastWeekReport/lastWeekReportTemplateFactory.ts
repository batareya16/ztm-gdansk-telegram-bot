import LastWeekReportTemplate from "./lastWeekReportTemplate";

export default class LastWeekReportTemplateFactory {
  public static async constructTemplate(): Promise<LastWeekReportTemplate> {
    return new LastWeekReportTemplate();
  }
}
