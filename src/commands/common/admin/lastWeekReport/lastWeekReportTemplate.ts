import CommandTemplate from "../../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {constructStatsKeyboard} from "../adminTemplateHelper";
import {AdminCommandType} from "../adminCommandType";
import {addDays, formatDate} from "../../../infrastructure/dateHelper";

export default class LastWeekReportTemplate implements CommandTemplate {
  constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    return constructStatsKeyboard(callbackName, AdminCommandType.LastWeekReport);
  }

  constructMessage(): string {
    return `*Users requests report from ${formatDate(addDays(new Date(), -7))} to ${formatDate(new Date())}*`;
  }
}
