import WorkerLogsTemplate from "./workerLogsTemplate";

export default class WorkerLogsTemplateFactory {
  public static async constructTemplate(): Promise<WorkerLogsTemplate> {
    return new WorkerLogsTemplate();
  }
}
