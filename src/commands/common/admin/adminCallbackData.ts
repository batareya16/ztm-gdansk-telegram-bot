import {AdminCommandType} from "./adminCommandType";

export class AdminCallbackData {
  type!: AdminCommandType;
}
