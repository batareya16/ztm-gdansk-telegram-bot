import CommandsListTemplate from "../commandsList/commandsListTemplate";
import LastWeekRequestsTemplate from "./lastWeekRequestsTemplate";
import UserRequests from "./UserRequests";

export default class LastWeekRequestsTemplateFactory {
  public static async constructTemplate(usersRequests: UserRequests[]): Promise<CommandsListTemplate> {
    return new LastWeekRequestsTemplate(usersRequests);
  }
}
