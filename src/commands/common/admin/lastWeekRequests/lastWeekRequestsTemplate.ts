import CommandTemplate from "../../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {addDays, formatDate} from "../../../infrastructure/dateHelper";
import UserRequests from "./UserRequests";
import {constructStatsKeyboard} from "../adminTemplateHelper";
import {AdminCommandType} from "../adminCommandType";

export default class LastWeekRequestsTemplate implements CommandTemplate{
  public usersRequests: UserRequests[];

  constructor(usersRequests: UserRequests[]) {
    this.usersRequests = usersRequests;
  }

  constructKeyboard(callbackName: string): Promise<InlineKeyboard | null> {
    return constructStatsKeyboard(callbackName, AdminCommandType.LastWeekRequests);
  }

  constructMessage(): string {
    return `*Users requests from ${formatDate(addDays(new Date(), -7))} to ${formatDate(new Date())}*:
    ${this.usersRequests
      .map(req => `
Full Name: ${req.FirstName} ${req.LastName}
Username: ${req.UserName} ID: ${req.UserId} Requests: ${req.RequestsCount}`)
      .join('\n')}`;
  }
}
