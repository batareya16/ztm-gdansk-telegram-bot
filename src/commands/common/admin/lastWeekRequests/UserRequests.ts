export default class UserRequests {
  UserName!: string;
  FirstName!: string;
  LastName!: string;
  UserId!: number;
  RequestsCount!: number;
}
