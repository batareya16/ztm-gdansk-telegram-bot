import CommandTemplate from "../../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {constructStatsKeyboard} from "../adminTemplateHelper";
import {AdminCommandType} from "../adminCommandType";
import { readTextFromFile } from "../../../../infrastructure/helpers/fileHelper";

export default class BotLogsTemplate implements CommandTemplate{

  constructor() {
  }

  constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    return constructStatsKeyboard(callbackName, AdminCommandType.BotLogs);
  }

  constructMessage(): string {
    return `\`${readTextFromFile("/home/pi/botlogs.txt")}\``;
  }
}
