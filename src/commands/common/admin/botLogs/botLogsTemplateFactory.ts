import BotLogsTemplate from "./botLogsTemplate";

export default class BotLogsTemplateFactory {
  public static async constructTemplate(): Promise<BotLogsTemplate> {
    return new BotLogsTemplate();
  }
}
