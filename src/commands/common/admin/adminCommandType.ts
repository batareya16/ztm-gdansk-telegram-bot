export enum AdminCommandType {
  CommandsList,
  AllUsersCount,
  LastWeekRequests,
  LastWeekReport,
  WorkerLogs,
  BotLogs,
  TriggerWorker
}
