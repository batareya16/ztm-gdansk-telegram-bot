import MessageCommand from "../../../infrastructure/commands/messageCommand";
import BotWrapper from "../../../infrastructure/BotWrapper";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import AllBotsTemplateFactory from "./allBotsTemplateFactory";
import AllBotsCallbackData from "./allBotsCallbackData";

export default class AllBotsCommand implements MessageCommand {
  getCallBackName(): string {
    return "ab";
  }

  async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    const callbackData: AllBotsCallbackData = callback.data;
    const template = AllBotsTemplateFactory.constructTemplate(callbackData.loc);
    await bot.editMessageTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const template = AllBotsTemplateFactory.constructTemplate(bot.getLanguageCode());
    await bot.sendTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  validateInput(input: string): boolean {
    return input === "/allbots";
  }
}
