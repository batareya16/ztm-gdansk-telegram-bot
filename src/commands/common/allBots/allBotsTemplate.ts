import CommandTemplate from "../../infrastructure/commandTemplate";
import InlineKeyboard from "../../../infrastructure/telebot/InlineKeyboard";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import InlineKeyboardButton from "../../../infrastructure/telebot/InlineKeyboardButton";
import {InlineKeyboardType} from "../../../infrastructure/telebot/InlineKeyboardType";
import {getTranslation} from "../../../infrastructure/helpers/resourceHelper";

export default class AllBotsTemplate implements CommandTemplate {
  private readonly userLocale: string;

  constructor(userLocale: string) {
    this.userLocale = userLocale;
  }

  constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    const callback = new CommandCallback();
    callback.name = callbackName;

    return Promise.resolve(new InlineKeyboard([
      new InlineKeyboardButton("🇬🇧", JSON.stringify({...callback, data: {loc: "en"}})),
      new InlineKeyboardButton("🇷🇺", JSON.stringify({...callback, data: {loc: "ru"}})),
      new InlineKeyboardButton("🇵🇱", JSON.stringify({...callback, data: {loc: "pl"}}))
    ], InlineKeyboardType.Horizontal));
  }

  constructMessage(): string {
    return getTranslation(this.userLocale, "allbots_description", true);
  }
}
