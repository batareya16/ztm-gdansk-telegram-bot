import AllBotsTemplate from "./allBotsTemplate";

export default class AllBotsTemplateFactory {
  public static constructTemplate(userLocale: string): AllBotsTemplate {
    return new AllBotsTemplate(userLocale);
  }
}
