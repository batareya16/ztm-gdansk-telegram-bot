import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import StopInfoCallbackData from "../stopInfoCallbackData";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import CommandTemplate from "../../../infrastructure/commandTemplate";

export default class SimilarStopListTemplate implements CommandTemplate {
  private stops: TemplateStop[] = [];

  public addStop(name: string, id: number) {
    this.stops.push(new TemplateStop(name, id));
  }

  public constructMessage(): string {
    return "What do you mean?";
  }

  public constructKeyboard(callbackName: string): Promise<InlineKeyboard | null> {
    const callback: CommandCallback = new CommandCallback();
    callback.name = callbackName;

    return Promise.resolve(new InlineKeyboard(this.stops.map(stop => {
      const callbackData = new StopInfoCallbackData();
      callbackData.stId = stop.stopId;
      callbackData.st = 1;
      return new InlineKeyboardButton(stop.stopName, JSON.stringify({...callback, data: callbackData}));
    }), InlineKeyboardType.Vertical));
  }
}

class TemplateStop {
  stopName: string;
  stopId: number;

  constructor(stopName: string, stopId: number) {
    this.stopName = stopName;
    this.stopId = stopId;
  }
}
