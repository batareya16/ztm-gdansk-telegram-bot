import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import CommandTemplate from "../../../infrastructure/commandTemplate";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import StopInfoCallbackData from "../stopInfoCallbackData";

export default class StopInfoMessageTemplate implements CommandTemplate {

  private stopInfo: TemplateStop;

  constructor(id: number, name: string) {
    this.stopInfo = new TemplateStop(id, name);
  }

  public addTrip(
    id: number,
    shortName: string,
    headSign: string,
    arrivalTime: Date,
    departureTime: Date) {
    this.stopInfo.trips.push(new TemplateTrip(id, shortName, headSign, arrivalTime, departureTime));
  }

  public constructMessage(): string {
    return `
*${this.stopInfo.stopName}*
\`\`\`
${this.stopInfo.trips.map((route, index) => { return `
  🚆 ${index+1}: ${route.headSign}
  ${this.toClockNum(route.arrivalTime.getHours())}:${this.toClockNum(route.arrivalTime.getMinutes())} - ${this.toClockNum(route.departureTime.getHours())}:${this.toClockNum(route.departureTime.getMinutes())}`
    }).join("")}
\`\`\``
  }

  public async constructKeyboard(callbackName: string) : Promise<InlineKeyboard> {
    const callback = new CommandCallback();
    callback.name = callbackName;

    const callbackData = new StopInfoCallbackData();
    callbackData.st = 2;
    return new InlineKeyboard(this.stopInfo.trips.map((trip, index) => {
      return new InlineKeyboardButton(`${index+1}`, JSON.stringify({...callback, data: {...callbackData, stId: this.stopInfo.stopId, trId: trip.id}}))
    }), InlineKeyboardType.Horizontal);
  }

  private toClockNum(numberStr: number): string {
    return numberStr.toString().padStart(2, "0");
  }
}

export class TemplateStop {
  stopId: number;
  stopName: string;
  trips: TemplateTrip[] = []

  constructor(id: number, name: string) {
    this.stopId = id;
    this.stopName = name;
  }
}

export class TemplateTrip {
  id: number;
  headSign: string;
  shortName: string;
  arrivalTime: Date;
  departureTime: Date;


  constructor(
    id: number,
    shortName: string,
    headSign: string,
    arrivalTime: Date,
    departureTime: Date) {
    this.id = id;
    this.shortName = shortName;
    this.headSign = headSign;
    this.arrivalTime = arrivalTime;
    this.departureTime = departureTime;
  }
}
