import StopInfoMessageTemplate from "./stopInfoMessageTemplate";
import Stop from "../../../../data/skm_trojmiasto/models/stop";
import StopTimeRepository from "../../../../data/skm_trojmiasto/repositories/stopTimeRepository";
import TripRepository from "../../../../data/skm_trojmiasto/repositories/tripRepository";
import StopTime from "../../../../data/skm_trojmiasto/models/stopTime";
import Trip from "../../../../data/skm_trojmiasto/models/trip";

export default class ExactStopInfoTemplateFactory {
  private stopTimeRepository: StopTimeRepository = new StopTimeRepository();
  private tripRepository: TripRepository = new TripRepository();

  public async constructTemplate(stop: Stop): Promise<StopInfoMessageTemplate> {
    const stopInfoMessageTemplate: StopInfoMessageTemplate = new StopInfoMessageTemplate(stop.Id, stop.Name);
    let stopTimes = await this.stopTimeRepository.findNearestStopTimes(stop.Id);
    for (let i = 0; i < stopTimes.rows.length; i++) {
      let stopTime: StopTime = stopTimes.rows[i];
      let trip: Trip = (await this.tripRepository.findTrip(stopTime.TripId)).rows[0];
      stopInfoMessageTemplate.addTrip(trip.Id, trip.ShortName, trip.HeadSign, stopTime.ArrivalTime, stopTime.DepartureTime);
    }

    return stopInfoMessageTemplate;
  }
}
