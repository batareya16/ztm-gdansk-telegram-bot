import BotWrapper from "../../../infrastructure/BotWrapper";
import MessageCommand from "../../../infrastructure/commands/messageCommand";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import ExactStopInfoTemplateFactory from "./exactStop/exactStopInfoTemplateFactory";
import StopInfoCallbackData from "./stopInfoCallbackData";
import SimilarStopListTemplateFactory from "./similarStopList/similarStopListTemplateFactory";
import Stop from "../../../data/skm_trojmiasto/models/stop";
import {StopRepository} from "../../../data/skm_trojmiasto/repositories/stopRepository";
import TripInfoTemplateFactory from "./tripInfo/tripInfoTemplateFactory";
import TripRepository from "../../../data/skm_trojmiasto/repositories/tripRepository";
import StopTimeRepository from "../../../data/skm_trojmiasto/repositories/stopTimeRepository";

export default class StopInfoCommand implements MessageCommand {

  private stopsRepository: StopRepository = new StopRepository();
  private tripsRepository: TripRepository = new TripRepository();
  private stopTimesRepository: StopTimeRepository = new StopTimeRepository();

  public async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const stops = await this.stopsRepository.findStopsByName(input);
    if (stops.rowCount > 0) {
      const template = await new ExactStopInfoTemplateFactory().constructTemplate(stops.rows[0]);
      await bot.sendTextWithKeyboard(
        template!.constructMessage(),
        await template!.constructKeyboard(this.getCallBackName()));
      return;
    }

    const similarStops = await this.stopsRepository.findStopsWithSimilarName(input);
    const template = await new SimilarStopListTemplateFactory().constructTemplate(similarStops.rows);
    await bot.sendTextWithKeyboard(
      template!.constructMessage(),
      await template!.constructKeyboard(this.getCallBackName()));
  }

  public async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    const callbackData: StopInfoCallbackData = callback.data;

    const stop: Stop = (await this.stopsRepository.findStopsById(callbackData.stId)).rows[0];
    switch (callbackData.st) {
      case 1:
        const exactStopTemplate = await new ExactStopInfoTemplateFactory().constructTemplate(stop);
        await bot.editMessageTextWithKeyboard(
          exactStopTemplate.constructMessage(),
          await exactStopTemplate.constructKeyboard(this.getCallBackName()));
        break;
      case 2:
        const trip = (await this.tripsRepository.findTrip(callbackData.trId)).rows[0];
        const stopTimes = (await this.stopTimesRepository.findTripStopTimes(trip.Id)).rows;
        const stops = (await this.stopsRepository.findStopsByIds(stopTimes.map(st => st.StopId))).rows;
        const tripInfoTemplate = await new TripInfoTemplateFactory().constructTemplate(stop, trip, stopTimes, stops);
        await bot.editMessageTextWithKeyboard(
          tripInfoTemplate.constructMessage(),
          await tripInfoTemplate.constructKeyboard(this.getCallBackName()));
    }
  }

  validateInput(input: string): boolean {
    return !new RegExp("^[nN]?[0-9]+$").test(input) &&
      new RegExp("^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9 .,]+$").test(input);
  }

  getCallBackName(): string { return "si"; }
}
