import StopTime from "../../../../data/skm_trojmiasto/models/stopTime";
import TripInfoTemplate from "./tripInfoTemplate";
import Stop from "../../../../data/skm_trojmiasto/models/stop";
import Trip from "../../../../data/skm_trojmiasto/models/trip";

export default class TripInfoTemplateFactory {
  public constructTemplate(sourceStop: Stop, trip: Trip, stopTimes: StopTime[], stops: Stop[]): TripInfoTemplate {
    const stopMap = new Map(stops.map(stop => [stop.Id, stop]));
    const template = new TripInfoTemplate(sourceStop.Id, trip.HeadSign);
    stopTimes.forEach(x => {
      const stop = stopMap.get(x.StopId)!;
      template.addStopTime(stop.Name, stop.Id, x.ArrivalTime, x.DepartureTime, x.Sequence);
    });

    return template;
  }
}
