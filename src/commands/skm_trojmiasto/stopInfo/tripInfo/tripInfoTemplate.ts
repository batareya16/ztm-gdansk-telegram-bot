import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import StopInfoCallbackData from "../stopInfoCallbackData";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import CommandTemplate from "../../../infrastructure/commandTemplate";

export default class TripInfoTemplate implements CommandTemplate {
  private stopTimes: TemplateStopTime[] = [];
  private sourceStopId: number;
  private headSign: string;

  constructor(sourceStopId: number, headSign: string) {
    this.sourceStopId = sourceStopId;
    this.headSign = headSign;
  }

  public addStopTime(stopName: string, stopId: number, arrivalTime: Date, departureTime: Date, sequence: number) {
    this.stopTimes.push(new TemplateStopTime(stopName, stopId, arrivalTime, departureTime, sequence));
  }

  public constructMessage(): string {
    return `*🚆→ ${this.headSign}*
\`\`\`
${this.stopTimes.sort(TripInfoTemplate.sortStopTimes).map(stopTime => `
${stopTime.stopId==this.sourceStopId ? "\`\`\`_*" : ""}${this.toClockNum(stopTime.arrivalTime.getHours())}:${this.toClockNum(stopTime.arrivalTime.getMinutes())}: ${stopTime.stopName}${stopTime.stopId==this.sourceStopId ? "*_\n\`\`\`" : ""}`).join('')}
\`\`\``;
  }

  public constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    const callback = new CommandCallback();
    callback.name = callbackName;
    const backCallbackData = new StopInfoCallbackData();
    backCallbackData.st = 1;
    backCallbackData.stId = this.sourceStopId;
    return Promise.resolve(new InlineKeyboard([
      new InlineKeyboardButton("🔙", JSON.stringify({...callback, data: backCallbackData}))
    ], InlineKeyboardType.Horizontal));
  }

  private toClockNum(numberStr: number): string {
    return numberStr.toString().padStart(2, "0");
  }

  private static sortStopTimes(a: TemplateStopTime, b: TemplateStopTime): number {
    const aCode: number = +(a.sequence);
    const bCode: number = +(b.sequence);
    return aCode > bCode ? 1 : -1;
  }
}

class TemplateStopTime {
  stopName: string;
  stopId: number;
  arrivalTime: Date;
  departureTime: Date;
  sequence: number;

  constructor(stopName: string, stopId: number, arrivalTime: Date, departureTime: Date, sequence: number) {
    this.stopName = stopName;
    this.stopId = stopId;
    this.arrivalTime = arrivalTime;
    this.departureTime = departureTime;
    this.sequence = sequence;
  }
}
