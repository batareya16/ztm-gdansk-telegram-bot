import Route from "../../../../data/ztm_gdansk/models/route";

export default class ExtendedRouteInfo extends Route{
  isReversedDirections!: boolean;
}
