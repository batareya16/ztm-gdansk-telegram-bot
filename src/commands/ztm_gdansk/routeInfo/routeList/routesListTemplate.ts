import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import RouteDirectionCallbackData from "./routeDirectionCallbackData";
import CommandTemplate from "../../../infrastructure/commandTemplate";

export default class RoutesListTemplate implements CommandTemplate{
  private routes: TemplateRoute[] = [];

  public addRoute(id: number, shortName: string): void {
    this.routes.push(new TemplateRoute(id, shortName));
  }

  public addDirection(id: number, description: string): void {
    this.routes[this.routes.length - 1].addDirection(id, description);
  }

  public constructMessage(): string {
    return "Choose a direction:"
  }

  public async constructKeyboard(callbackName: string) : Promise<InlineKeyboard | null> {
    const callback = new CommandCallback();
    callback.name = callbackName;
    if (this.routes.length === 0) {
      return null;
    }

    return new InlineKeyboard(this.routes.map(route => {
      return route.directions.map(dir => {
        const endStops = dir.description.split('-').map(x => x.trim());
        const callbackData = new RouteDirectionCallbackData();
        callbackData.st = 1;
        callbackData.id = +route.id;
        callbackData.dir = +dir.id;
        return new InlineKeyboardButton(endStops[1],  JSON.stringify({...callback, data: callbackData}));
      })
    }).reduce((a,b) => a.concat(b)),
      InlineKeyboardType.Vertical);
  }
}

class TemplateRoute {
  id: number;
  shortName: string;
  directions: TemplateDirection[] = [];

  constructor(id: number, shortName: string) {
    this.id = id;
    this.shortName = shortName;
  }

  public addDirection(id: number, description: string): void {
    this.directions.push(new TemplateDirection(id, description));
  }
}

class TemplateDirection {
  id: number;
  description: string;

  constructor(id: number, description: string) {
    this.id = id;
    this.description = description;
  }
}
