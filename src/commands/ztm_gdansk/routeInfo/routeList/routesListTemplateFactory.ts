import RoutesListTemplate from "./routesListTemplate";
import {getTypeSymbol, reverseStops} from "../../../infrastructure/routeHelper";
import ExtendedRouteInfo from "./ExtendedRouteInfo";

export default class RoutesListTemplateFactory {
  public static async constructTemplate(routes: ExtendedRouteInfo[]): Promise<RoutesListTemplate> {
    const template = new RoutesListTemplate();
    routes.forEach(route => {
      template.addRoute(route.Id, `${getTypeSymbol(route.Type)} ${route.ShortName}`);
      template.addDirection(route.isReversedDirections ? 2 : 1, route.LongName)
      template.addDirection(route.isReversedDirections ? 1 : 2, reverseStops(route.LongName))
    });

    return template;
  }
}
