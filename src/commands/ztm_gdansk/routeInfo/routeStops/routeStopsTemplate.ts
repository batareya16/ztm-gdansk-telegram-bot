import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import RouteStopCallbackData from "./routeStopCallbackData";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import {getTypeSymbol} from "../../../infrastructure/routeHelper";
import CommandTemplate from "../../../infrastructure/commandTemplate";
import Route from "../../../../data/ztm_gdansk/models/route";

export default class RouteStopsTemplate implements CommandTemplate {
  private route: Route;
  private stops: TemplateStop[] = [];
  private direction: number;

  constructor(route: Route, direction: number) {
    this.route = route;
    this.direction = direction;
    this.route = route;
  }

  public addStop(name: string, id: number) {
    this.stops.push(new TemplateStop(name, id));
  }

  public constructMessage(): string {
    return `Choose a stop for the route ${this.route.ShortName} ${getTypeSymbol(this.route.Type)}:
\\*️⃣ - stop for an additional route`;
  }

  public async constructKeyboard(callbackName: string): Promise<InlineKeyboard> {
    const callback = new CommandCallback();
    callback.name = callbackName;

    const callbackData = new RouteStopCallbackData();
    callbackData.st = 2;
    callbackData.rt = +this.route.Id;
    callbackData.dir = this.direction;
    return new InlineKeyboard(this.stops.map(stop => {
      return new InlineKeyboardButton(stop.name, JSON.stringify({...callback, data: {...callbackData, id: +stop.id}}))
    }), InlineKeyboardType.Vertical);
  }
}

class TemplateStop {
  name: string;
  id: number;

  constructor(name: string, id: number) {
    this.name = name;
    this.id = id;
  }
}
