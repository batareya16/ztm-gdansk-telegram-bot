import RouteStopsTemplate from "./routeStopsTemplate";
import RouteStop from "../../../../data/ztm_gdansk/models/routeStop";
import Route from "../../../../data/ztm_gdansk/models/route";

export default class RouteStopsTemplateFactory {
  public static async constructTemplate(
    route: Route,
    direction: number,
    distinctStops: RouteStop[],
    mainRouteStops: RouteStop[]): Promise<RouteStopsTemplate> {
      const template = new RouteStopsTemplate(route, direction);
      distinctStops.forEach(stop => {
        template.addStop(
          mainRouteStops.find(x => x.StopName === stop.StopName) !== undefined
              ? stop.StopName
              : `${stop.StopName} \*️⃣`,
          stop.StopId);
      });

      return template;
  }
}
