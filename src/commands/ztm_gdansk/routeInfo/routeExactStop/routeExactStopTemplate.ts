import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import RouteDirectionCallbackData from "../routeList/routeDirectionCallbackData";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import RouteStopCallbackData from "../routeStops/routeStopCallbackData";
import {addDays, formatDate} from "../../../infrastructure/dateHelper";
import CommandTemplate from "../../../infrastructure/commandTemplate";
import Stop from "../../../../data/ztm_gdansk/models/stop";

export default class RouteExactStopTemplate implements CommandTemplate {
  private routeStop: TemplateRouteStop;
  private indexLetters = "¹²³⁴⁵⁶⁷⁸⁹";

  constructor(routeId: number, shortName: string, mainTripId: number, direction: number, stop: Stop) {
    this.routeStop = new TemplateRouteStop(routeId, shortName, mainTripId, direction, stop);
  }

  public addStopTime(time: Date, tripId: number, isLive: boolean) {
    this.routeStop.stopTimes.push(new TemplateStopTime(time, tripId, isLive));
  }

  public addTrip(id: number, description: string, type: string) {
    this.routeStop.trips.push(new TemplateStopTrip(id, description, type));
  }

  public constructMessage(): string {
    const sideTrips = this.routeStop.trips.filter(x => x.id != this.routeStop.mainTripId);
    const days: string[][] = [];
    const maxDays = 2;
    const stopTimesToProcess = this.routeStop.stopTimes.filter(x => x.time >= new Date() && x.time <= addDays(new Date(), maxDays - 1));
    for (let i = 0; i < maxDays; i++) {
      const minutes: string[] = new Array(24).fill("");
      stopTimesToProcess.filter(x => x.time.getDate().valueOf() == addDays(new Date(), i).getDate().valueOf())
        .forEach((time: TemplateStopTime) => {
          minutes[time.time.getHours()] +=
            (time.isLive ? "⚡" : "") +
            RouteExactStopTemplate.pad(time.time.getMinutes(), 2) +
            (this.indexLetters[sideTrips.indexOf(sideTrips.find(x => x.id === time.tripId)!)] || "") +
            ' '
      });

      days.push(minutes);
    }

    return `
*${this.routeStop.stop.Name} ${this.routeStop.stop.Code}*

${RouteExactStopTemplate.removeStopId(this.routeStop.trips.find(x => x.id == this.routeStop.mainTripId)?.description) || ""}
${sideTrips.map((val, index) =>
      `${this.indexLetters[index]} - ${RouteExactStopTemplate.removeStopId(val.description)}`).join("\n")}
\`\`\`
${days.map(((day, dayIndex) => {
  return `
${formatDate(addDays(new Date(), dayIndex))}
${day.map((val, index) => val === "" ? "" : `${RouteExactStopTemplate.pad(index, 2)}: ${val}\n`).join("")}`
    })).join("")}
\`\`\``
  }

  private static pad(num: number, size: number) {
    let strVal = num.toString();
    while (strVal.length < size) {
      strVal = "0" + num;
    }

    return strVal;
  }

  private static removeStopId(route: string | undefined) {
    if (route === undefined) {
      return "";
    }

    return route.replace(/ *\([^)]*\)*/g, "");
  }

  public async constructKeyboard(callbackName: string) : Promise<InlineKeyboard> {
    const callback = new CommandCallback();
    callback.name = callbackName;
    const listCallbackData = new RouteDirectionCallbackData();
    listCallbackData.st = 1;
    listCallbackData.id = this.routeStop.routeId;
    listCallbackData.dir = this.routeStop.direction;
    const routeStopCallbackData = new RouteStopCallbackData();
    routeStopCallbackData.st = 2;
    routeStopCallbackData.rt = this.routeStop.routeId;
    routeStopCallbackData.id = this.routeStop.stop.Id;
    routeStopCallbackData.dir = this.routeStop.direction;
    return new InlineKeyboard([
      new InlineKeyboardButton("🔄", JSON.stringify({...callback, data: routeStopCallbackData})),
      new InlineKeyboardButton("🔙", JSON.stringify({...callback, data: listCallbackData}))
    ], InlineKeyboardType.Horizontal);
  }
}

class TemplateRouteStop {
  routeId: number;
  shortName: string;
  direction: number;
  mainTripId: number;
  stop: Stop;
  trips: TemplateStopTrip[] = [];
  stopTimes: TemplateStopTime[] = [];

  constructor(routeId: number, shortName: string, mainTripId: number, direction: number, stop: Stop) {
    this.shortName = shortName;
    this.mainTripId = mainTripId;
    this.routeId = routeId;
    this.direction = direction;
    this.stop = stop;
  }

  public addStopTime(time: Date, tripId: number, isLive: boolean) {
    this.stopTimes.push(new TemplateStopTime(time, tripId, isLive));
  }

  public addTrip(id: number, endStop: string, type: string) {
    this.trips.push(new TemplateStopTrip(id, endStop, type));
  }
}

class TemplateStopTrip {
  id: number;
  description: string;
  type: string;

  constructor(id: number, description: string, type: string) {
    this.id = id;
    this.description = description;
    this.type = type;
  }
}

class TemplateStopTime {
  time: Date;
  tripId: number;
  isLive: boolean;

  constructor(time: Date, tripId: number, isLive: boolean) {
    this.time = time;
    this.tripId = tripId;
    this.isLive = isLive;
  }
}


