import RouteExactStopTemplate from "./routeExactStopTemplate";
import {StopInfoResponse} from "../../../infrastructure/stopInfoApi";
import Stop from "../../../../data/ztm_gdansk/models/stop";
import Trip from "../../../../data/ztm_gdansk/models/trip";
import StopTime from "../../../../data/ztm_gdansk/models/stopTime";

export default class RouteExactStopTemplateFactory {
  public static async constructTemplate(
    stop: Stop,
    routeId: number,
    shortName: string,
    direction: number,
    mainTripId: number,
    trips: Trip[],
    stopTimes: StopTime[],
    stopInfo: StopInfoResponse) : Promise<RouteExactStopTemplate> {
    const template = new RouteExactStopTemplate(routeId, shortName, mainTripId, direction, stop);
    trips.forEach(x => template.addTrip(x.TripId, x.Headsign, x.RouteType));

    const tripIds = trips.map(x => +x.TripId);
    const validDelays = stopInfo.delay.filter(x => tripIds.includes(x.tripId) && x.routeId === +routeId);
    const delayTimes = validDelays.map(x => {
      const time = RouteExactStopTemplateFactory.parseTime(x.theoreticalTime);
      time.setDate(new Date().getDate());
      return RouteExactStopTemplateFactory.getFilterIndex(time);
    });
    const actualDates = stopTimes.filter(x => {
      x.ArrivalTime.setDate(x.ValidDate.getDate());
      const minutes = RouteExactStopTemplateFactory.getFilterIndex(x.ArrivalTime);
      return minutes >= RouteExactStopTemplateFactory.getFilterIndex(new Date()) && delayTimes.indexOf(minutes) === -1;
    });

    validDelays.forEach(x => template.addStopTime(RouteExactStopTemplateFactory.parseTime(x.estimatedTime), x.tripId, true));
    actualDates.forEach(x => {
      x.ArrivalTime.setFullYear(x.ValidDate.getFullYear(), x.ValidDate.getMonth(), x.ValidDate.getDate())
      template.addStopTime(x.ArrivalTime, x.TripId, false)
    });

    return template;
  }

  private static getFilterIndex(date: Date){
    return date.getMinutes() + date.getHours() * 60 + date.getDate() * 10000;
  }

  private static parseTime(t: string): Date {
    const d = new Date();
    const time = t.match(/(\d+)(?::(\d\d))?\s*(p?)/);
    d.setHours( parseInt( time![1]) + (time![3] ? 12 : 0) );
    d.setMinutes( parseInt( time![2]) || 0 );
    return d;
  }
}
