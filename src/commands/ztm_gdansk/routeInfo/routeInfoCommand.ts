import MessageCommand from "../../../infrastructure/commands/messageCommand";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import BotWrapper from "../../../infrastructure/BotWrapper";
import {RouteRepository} from "../../../data/ztm_gdansk/repositories/routeRepository";
import RoutesListTemplateFactory from "./routeList/routesListTemplateFactory";
import RouteDirectionCallbackData from "./routeList/routeDirectionCallbackData";
import TripRepository from "../../../data/ztm_gdansk/repositories/tripRepository";
import RouteStopRepository from "../../../data/ztm_gdansk/repositories/routeStopRepository";
import RouteStop from "../../../data/ztm_gdansk/models/routeStop";
import RouteStopsTemplateFactory from "./routeStops/routeStopsTemplateFactory";
import RouteStopCallbackData from "./routeStops/routeStopCallbackData";
import StopTimeRepository from "../../../data/ztm_gdansk/repositories/stopTimeRepository";
import {Api, StopInfoResponse} from "../../infrastructure/stopInfoApi";
import {StopRepository} from "../../../data/ztm_gdansk/repositories/stopRepository";
import RouteExactStopTemplateFactory from "./routeExactStop/routeExactStopTemplateFactory";
import {getEndStation} from "../../infrastructure/routeHelper";
import Trip from "../../../data/ztm_gdansk/models/trip";
import Route from "../../../data/ztm_gdansk/models/route";
import Stop from "../../../data/ztm_gdansk/models/stop";

export default class RouteInfoCommand implements MessageCommand {
  private routesRepository: RouteRepository = new RouteRepository();
  private tripsRepository: TripRepository = new TripRepository();
  private routeStopsRepository: RouteStopRepository = new RouteStopRepository();
  private stopTimeRepository: StopTimeRepository = new StopTimeRepository();
  private stopRepository: StopRepository = new StopRepository();

  async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    switch (callback.data.st) {
      case 1: await this.processRoutesListCallback(bot, callback.data); return;
      case 2: await this.processRoutesStopCallback(bot, callback.data); return;
    }
  }


  async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const routes = await this.routesRepository.findRoutesByShortName(input);
    for (const route of routes.rows) {
      const mainTrip: Trip = (await this.tripsRepository.findValidMainRouteTrip(route.Id)).rows[0];
      const mainTripEndStation = mainTrip.Headsign.split('-')[1].trim();
      const routeEndStation = getEndStation(route.LongName);
      route.isReversedDirections = !(routeEndStation.includes(mainTripEndStation) || mainTripEndStation.includes(routeEndStation));
    }

    const template = await RoutesListTemplateFactory.constructTemplate(routes.rows);
    await bot.sendTextWithKeyboard(
      template.constructMessage(),
      await template.constructKeyboard(this.getCallBackName()));
  }

  getCallBackName(): string { return "ri"; }

  validateInput(input: string): boolean {
    return new RegExp("^[nN]?[0-9]{1,5}$").test(input);
  }

  private async processRoutesListCallback(bot: BotWrapper, callbackData: RouteDirectionCallbackData): Promise<void> {
    const route: Route = (await this.routesRepository.findRoutesByIds([callbackData.id])).rows[0];
    const tripsQueryResult = await this.tripsRepository.findValidDirectedRouteTrips(callbackData.id, callbackData.dir);
    const trips: Trip[] = tripsQueryResult.rows;
    const stopsQueryResult = await this.routeStopsRepository.findTripsStops(callbackData.id, trips.map(x => x.TripId));

    const mainTripId = trips.find(x => x.RouteType == "MAIN")!.TripId;
    let distinctStops = stopsQueryResult.rows
      .filter((value: RouteStop, index: number, arr: RouteStop[]) =>
        arr.map(x => x.StopName).indexOf(value.StopName) == index)
      .sort((a, b) => a.Order - b.Order);

    const template = await RouteStopsTemplateFactory.constructTemplate(
      route,
      callbackData.dir,
      distinctStops,
      stopsQueryResult.rows.filter((x: RouteStop) => x.TripId == mainTripId));

    await bot.editMessageTextWithKeyboard(
      template!.constructMessage(),
      await template!.constructKeyboard(this.getCallBackName()));
  }

  private async processRoutesStopCallback(bot: BotWrapper, callbackData: RouteStopCallbackData) {
    const stop: Stop = (await this.stopRepository.findStopsById(callbackData.id)).rows[0]
    const trips: Trip[] = (await this.tripsRepository.findValidDirectedRouteTrips(callbackData.rt, callbackData.dir)).rows;
    const stopTimes = await this.stopTimeRepository.findStopTimeByTrips(callbackData.id, callbackData.rt, trips.map(x => x.TripId));
    const stopInfo: StopInfoResponse = await Api.getStopInfo(callbackData.id);

    const template = await RouteExactStopTemplateFactory.constructTemplate(
      stop,
      trips[0].RouteId,
      trips[0].ShortName,
      callbackData.dir,
      trips.find(x => x.RouteType == "MAIN")!.TripId,
      trips,
      stopTimes.rows,
      stopInfo);

    await bot.editMessageTextWithKeyboard(
      template!.constructMessage(),
      await template!.constructKeyboard(this.getCallBackName()));
  }
}
