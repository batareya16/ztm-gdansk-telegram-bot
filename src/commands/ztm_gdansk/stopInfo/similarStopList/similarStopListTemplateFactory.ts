import SimilarStopListTemplate from "./similarStopListTemplate";
import Stop from "../../../../data/ztm_gdansk/models/stop";

export default class SimilarStopListTemplateFactory {
  public constructTemplate(stops: Stop[]): SimilarStopListTemplate {
    const template = new SimilarStopListTemplate();
    stops.forEach(x => template.addStop(x.Name, x.Id));

    return template;
  }
}
