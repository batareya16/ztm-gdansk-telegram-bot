import {QueryResult} from "pg";
import {Api, StopInfoResponse} from "../../../infrastructure/stopInfoApi";
import {RouteRepository} from "../../../../data/ztm_gdansk/repositories/routeRepository";
import StopInfoMessageTemplate from "./stopInfoMessageTemplate";
import Stop from "../../../../data/ztm_gdansk/models/stop";
import Route from "../../../../data/ztm_gdansk/models/route";

export default class ExactStopInfoTemplateFactory {
  private routesRepository: RouteRepository = new RouteRepository();

  public async constructTemplate(stops: Stop[]): Promise<StopInfoMessageTemplate> {
    const stopInfoMessageTemplate: StopInfoMessageTemplate = new StopInfoMessageTemplate();
    await this.processStops(stops, stopInfoMessageTemplate);
    return stopInfoMessageTemplate;
  }

  private async processStops(stops: Stop[], template: StopInfoMessageTemplate): Promise<void> {
    for (let i = 0; i < stops.length; i++) {
      const stop: Stop = stops[i];
      const stopInfoResponse = await Api.getStopInfo(stop.Id);
      template.addStop(stop.Id, `${stop.Name} ${stop.Code}`, stop.Code);

      if (stopInfoResponse.delay.length > 0) {
        let routes = await this.routesRepository.findRoutesByIds(stopInfoResponse.delay.map(x => x.routeId));
        await this.processStopRoutes(stopInfoResponse, routes, template);
      }
    }
  }

  private async processStopRoutes(
    response: StopInfoResponse,
    routes: QueryResult<Route>,
    template: StopInfoMessageTemplate): Promise<void> {
    response.delay.forEach(delay => {
      const route: Route = routes.rows.find(x => x.Id == delay.routeId)!;
      template.addRoute(
        route.Id,
        route.ShortName,
        delay.headsign,
        route.Type,
        delay.estimatedTime,
        delay.theoreticalTime,
        delay.delayInSeconds);
    });
  }
}
