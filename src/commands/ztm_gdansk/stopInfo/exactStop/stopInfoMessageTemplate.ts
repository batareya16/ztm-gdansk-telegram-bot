import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import StopInfoCallbackData from "../stopInfoCallbackData";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import {InlineKeyboardType} from "../../../../infrastructure/telebot/InlineKeyboardType";
import {getTypeSymbol} from "../../../infrastructure/routeHelper";
import CommandTemplate from "../../../infrastructure/commandTemplate";

export default class StopInfoMessageTemplate implements CommandTemplate {
  private stopsInfo: TemplateStop[] = [];

  public addStop(id: number, name: string, code: string) {
    this.stopsInfo.push(new TemplateStop(id, name, code));
  }

  public addRoute(
    id: number,
    shortName: string,
    description: string,
    type: string,
    estimatedTime: string,
    theoreticalTime: string,
    delayInSec: number) {
    this.stopsInfo[this.stopsInfo.length-1].routes
      .push(new TemplateRoute(id, shortName, description, type, estimatedTime, theoreticalTime, delayInSec));
  }

  public constructMessage(): string {
    return this.stopsInfo.sort(StopInfoMessageTemplate.sortStops).map(stop => {
      return `
*${stop.stopName}*
\`\`\`
${stop.routes.map(route => { return `
  ${getTypeSymbol(route.type)} ${route.shortName}: ${route.description}
  ${route.estimatedTime} (${route.theoreticalTime} ${route.delayInSec > 0 ? "+" : "-"} ${(Math.abs(route.delayInSec)/60).toFixed(2)} min.)`
}).join("")}
\`\`\``}).join("")
  }

  public async constructKeyboard(callbackName: string) : Promise<InlineKeyboard> {
    const callback: CommandCallback = new CommandCallback();
    callback.name = callbackName;
    const allCallbackData: StopInfoCallbackData = new StopInfoCallbackData();
    allCallbackData.id = this.stopsInfo[0].stopId;
    allCallbackData.all = true;

    return new InlineKeyboard(this.stopsInfo.sort(StopInfoMessageTemplate.sortStops).map(stop => {

        const callbackData: StopInfoCallbackData = new StopInfoCallbackData();
        callbackData.id = stop.stopId;
        callbackData.all = false;

        return new InlineKeyboardButton(stop.stopCode, JSON.stringify({...callback, data: callbackData}))
      }).concat(new InlineKeyboardButton("All", JSON.stringify({...callback, data: allCallbackData}))),
      InlineKeyboardType.Horizontal);
  }

  private static sortStops(a: TemplateStop, b: TemplateStop): number {
    const aCode: number = +(a.stopCode);
    const bCode: number = +(b.stopCode);
    return aCode > bCode ? 1 : -1;
  }
}

class TemplateStop {
  stopId: number;
  stopCode: string;
  stopName: string;
  routes: TemplateRoute[] = []

  constructor(id: number, name: string, code: string) {
    this.stopId = id;
    this.stopName = name;
    this.stopCode = code;
  }
}

class TemplateRoute {
  id: number;
  description: string;
  type: string;
  estimatedTime: string;
  theoreticalTime: string;
  delayInSec: number;
  shortName: string;


  constructor(
    id: number,
    shortName: string,
    description: string,
    type: string,
    estimatedTime: string,
    theoreticalTime: string,
    delayInSec: number) {
    this.id = id;
    this.shortName = shortName;
    this.description = description;
    this.estimatedTime = estimatedTime;
    this.theoreticalTime = theoreticalTime;
    this.delayInSec = delayInSec;
    this.type = type;
  }
}
