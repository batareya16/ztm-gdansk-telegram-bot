import BotWrapper from "../../../infrastructure/BotWrapper";
import {StopRepository} from "../../../data/ztm_gdansk/repositories/stopRepository";
import MessageCommand from "../../../infrastructure/commands/messageCommand";
import CommandCallback from "../../../infrastructure/commands/commandCallback";
import ExactStopInfoTemplateFactory from "./exactStop/exactStopInfoTemplateFactory";
import StopInfoCallbackData from "./stopInfoCallbackData";
import {QueryResult} from "pg";
import SimilarStopListTemplateFactory from "./similarStopList/similarStopListTemplateFactory";
import Stop from "../../../data/ztm_gdansk/models/stop";

export default class StopInfoCommand implements MessageCommand {

  private stopsRepository: StopRepository = new StopRepository();

  public async processMessage(bot: BotWrapper, input: string): Promise<void> {
    const stops = await this.stopsRepository.findStopsByName(input);
    if (stops.rowCount > 0) {
      const template = await new ExactStopInfoTemplateFactory().constructTemplate(stops.rows);
      await bot.sendTextWithKeyboard(
        template!.constructMessage(),
        await template!.constructKeyboard(this.getCallBackName()));
      return;
    }

    const similarStops = await this.stopsRepository.findStopsWithSimilarName(input);
    const template = await new SimilarStopListTemplateFactory().constructTemplate(similarStops.rows);
    await bot.sendTextWithKeyboard(
      template!.constructMessage(),
      await template!.constructKeyboard(this.getCallBackName()));
  }

  public async processCallback(bot: BotWrapper, callback: CommandCallback): Promise<void> {
    const callbackData: StopInfoCallbackData = callback.data;
    const stops: QueryResult<Stop> = await this.stopsRepository.findStopsWithSameNameById(callbackData.id);
    const stopsToProcess: Stop[] = callbackData.all
      ? stops.rows
      : [stops.rows.find(x => x.Id == callbackData.id)!];

    const messageTemplate = await new ExactStopInfoTemplateFactory().constructTemplate(stopsToProcess);
    const keyboardTemplate = await new ExactStopInfoTemplateFactory().constructTemplate(stops.rows);
    await bot.editMessageTextWithKeyboard(
      messageTemplate.constructMessage(),
      await keyboardTemplate!.constructKeyboard(this.getCallBackName()));
  }

  validateInput(input: string): boolean {
    return !new RegExp("^[nN]?[0-9]+$").test(input) &&
      new RegExp("^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9 .,]+$").test(input);
  }

  getCallBackName(): string { return "si"; }
}
