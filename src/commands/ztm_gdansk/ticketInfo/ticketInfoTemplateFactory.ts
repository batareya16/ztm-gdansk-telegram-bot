import TicketInfoTemplate from "./ticketInfoTemplate";
import Ticket from "../../../data/ztm_gdansk/models/ticket";

export default class TicketTemplateFactory {
  public static constructTemplate(tickets: Ticket[]): TicketInfoTemplate {
    return new TicketInfoTemplate(tickets);
  }
}
