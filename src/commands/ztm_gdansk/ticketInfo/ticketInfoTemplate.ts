import CommandTemplate from "../../infrastructure/commandTemplate";
import { formatDate } from "../../infrastructure/dateHelper";
import Ticket from "../../../data/ztm_gdansk/models/ticket";

export default class TicketInfoTemplate implements CommandTemplate {
    private tickets: Ticket[];

    constructor(tickets: Ticket[]) {
      this.tickets = tickets;
    }

    constructMessage(): string {
      return `*${this.tickets[0].Number}*
${this.tickets.map(ticket => {
  return `
*Expires: ${formatDate(ticket.ValidTo)}*
_*Type:* ${ticket.Description}_
_*Zone:* ${ticket.Zone}_`
      }).join("\n")}`
    }

    constructKeyboard(callbackName: string): Promise<null> {
      return Promise.resolve(null);
    }
}
