import TicketRepository from "../../../data/ztm_gdansk/repositories/ticketRepository";
import BotWrapper from "../../../infrastructure/BotWrapper";
import commandCallback from "../../../infrastructure/commands/commandCallback";
import MessageCommand from "../../../infrastructure/commands/messageCommand";
import TicketTemplateFactory from "./ticketInfoTemplateFactory";

export default class TicketInfoCommand implements MessageCommand {
    private ticketRepository: TicketRepository = new TicketRepository();

    async processMessage(bot: BotWrapper, input: string): Promise<void> {
      const tickets = await this.ticketRepository.findTicketsByNumber(input);
      let message = "404 Not found 😔";
      if (tickets.rows.length > 0) {
        message = TicketTemplateFactory.constructTemplate(tickets.rows).constructMessage();
      }

      await bot.sendTextMessage(message);
    }
    processCallback(bot: BotWrapper, callback: commandCallback): Promise<void> {
        throw new Error("Callbacks not supported in this command.");
    }
    validateInput(input: string): boolean {
      return new RegExp("[0-9]{10}").test(input);
    }
    getCallBackName(): string {
      return "ti";
    }
}
