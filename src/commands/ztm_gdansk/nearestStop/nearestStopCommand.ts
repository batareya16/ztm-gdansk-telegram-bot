import { StopRepository } from '../../../data/ztm_gdansk/repositories/stopRepository';
import BotWrapper from '../../../infrastructure/BotWrapper';
import BaseCommand from '../../../infrastructure/commands/baseCommand';
import StopInfoCommand from '../stopInfo/stopInfoCommand';
import NearestStopListTemplateFactory from './nearestStopList/nearestStopListTemplateFactory';

export default class NearestStopCommand implements BaseCommand {
  private stopsRepository: StopRepository = new StopRepository();

  public async processLocation(bot: BotWrapper, latitude: number, longitude: number): Promise<void> {
    const nearestStops = await this.stopsRepository.findNearestStops(latitude, longitude);
    const template = await new NearestStopListTemplateFactory().constructTemplate(nearestStops.rows);
    await bot.sendTextWithKeyboard(
      template!.constructMessage(),
      await template!.constructKeyboard(new StopInfoCommand().getCallBackName()));
  }

  getCallBackName(): string { return "ns"; }
}
