import NearestStopListTemplate from "./nearestStopListTemplate";
import Stop from "../../../../data/ztm_gdansk/models/stop";

export default class NearestStopListTemplateFactory {
  public constructTemplate(stops: Stop[]): NearestStopListTemplate {
    const template = new NearestStopListTemplate();
    stops.forEach(x => template.addStop(x.Name, x.Id));

    return template;
  }
}
