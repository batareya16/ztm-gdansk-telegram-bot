import CommandTemplate from "../../../infrastructure/commandTemplate";
import CommandCallback from "../../../../infrastructure/commands/commandCallback";
import InlineKeyboard from "../../../../infrastructure/telebot/InlineKeyboard";
import InlineKeyboardButton from "../../../../infrastructure/telebot/InlineKeyboardButton";
import { InlineKeyboardType } from "../../../../infrastructure/telebot/InlineKeyboardType";
import NearestStopCallbackData from "../nearestStopCallbackData";

export default class NearestStopListTemplate implements CommandTemplate {
  private stops: TemplateStop[] = [];

  public addStop(name: string, id: number) {
    this.stops.push(new TemplateStop(name, id));
  }

  public constructMessage(): string {
    return "Which stop is nearest to you?";
  }

  public constructKeyboard(callbackName: string): Promise<InlineKeyboard | null> {
    const callback: CommandCallback = new CommandCallback();
    callback.name = callbackName;

    return Promise.resolve(new InlineKeyboard(this.stops.map(stop => {
      const callbackData = new NearestStopCallbackData();
      callbackData.all = true;
      callbackData.id = stop.stopId;
      return new InlineKeyboardButton(stop.stopName, JSON.stringify({...callback, data: callbackData}));
    }), InlineKeyboardType.Vertical));
  }
}

class TemplateStop {
  stopName: string;
  stopId: number;

  constructor(stopName: string, stopId: number) {
    this.stopName = stopName;
    this.stopId = stopId;
  }
}
