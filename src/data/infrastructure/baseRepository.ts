import {QueryConfig, QueryResult} from "pg";
import {DbClient} from "./client";

export class BaseRepository {
  protected async executeQuery(query: QueryConfig): Promise<QueryResult> {
    const client = await DbClient.getClient();
    if (client == undefined) {
      throw new Error("client is undefined");
    }

    const result = await client.query(query);
    await client.end();
    return result;
  }
}
