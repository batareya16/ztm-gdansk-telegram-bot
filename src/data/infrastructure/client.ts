import { Client, ClientConfig } from 'pg';

export class DbClient {
  private static config: ClientConfig = {
    database: "skm",
    user: "batareya16",
    host: "localhost",
  }

  public static async getClient(): Promise<Client> {
    let client = new Client(this.config);
    await client.connect();
    return client;
  }
}
