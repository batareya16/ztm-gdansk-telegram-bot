class UserAction {
  UserId!: number;
  Timestamp!: number;
  ChatId!: number;
  FirstName!: string;
  LastName!: string;
  UserName!: string;
  LangCode!: string;
  Message!: string;
  Callback!: string;
}
