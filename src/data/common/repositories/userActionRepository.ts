import {BaseRepository} from "../../infrastructure/baseRepository";
import {QueryResult} from "pg";
import {addDays, getToday, getUnixTime} from "../../../commands/infrastructure/dateHelper";

export default class UserActionRepository extends BaseRepository {
  public async getLastUserAction(userId: number): Promise<QueryResult> {
    const query = {
      name: 'find-lastuserAction-byuserId',
      text: `SELECT * FROM useractions WHERE
        "UserId" = $1
        ORDER BY "Timestamp" DESC
        LIMIT 1`,
      values: [userId],
    }

    return this.executeQuery(query);
  }

  public async getUniqueUsersCount(): Promise<QueryResult> {
    const query = {
      name: 'count-uniqueusers',
      text: `SELECT COUNT(DISTINCT "UserId") as count FROM useractions`,
      values: [],
    }

    return this.executeQuery(query);
  }

  public async getLastWeekUsersRequests(): Promise<QueryResult> {
    const query = {
      name: 'get-lastweekuserrequests',
      text: `SELECT COUNT(*) as "RequestsCount", "UserId", "FirstName", "LastName", "UserName"
         FROM useractions
         WHERE "Timestamp" > $1
         GROUP BY "UserId", "FirstName", "LastName", "UserName"`,
      values: [getUnixTime(addDays(getToday(), -7))],
    }

    return this.executeQuery(query);
  }

  public async getLastWeekUserActions(): Promise<QueryResult> {
    const query = {
      name: 'get-lastweekuseractions',
      text: `SELECT * FROM useractions WHERE "Timestamp" > $1`,
      values: [getUnixTime(addDays(getToday(), -7))],
    }

    return this.executeQuery(query);
  }

  public async insertUserAction(item: UserAction) {
    const query = {
      name: 'insert-userAction',
      text: `INSERT INTO useractions(
      "UserId", "Timestamp", "ChatId", "FirstName", "LastName", "UserName", "LangCode", "Message", "Callback")
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
      values: [
        item.UserId,
        item.Timestamp,
        item.ChatId,
        item.FirstName,
        item.LastName,
        item.UserName,
        item.LangCode,
        item.Message,
        item.Callback
      ],
    }

    return this.executeQuery(query);
  }
}
