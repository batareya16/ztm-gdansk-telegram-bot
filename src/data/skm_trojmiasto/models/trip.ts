export default class Trip {
  ValidDate!: Date;
  Id!: number;
  RouteId!: number;
  ShortName!: string;
  HeadSign!: string;
}
