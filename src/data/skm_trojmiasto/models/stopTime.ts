export default class StopTime {
  TripId!: number;
  StopId!: number;
  ArrivalTime!: Date;
  DepartureTime!: Date;
  Sequence!: number;
}
