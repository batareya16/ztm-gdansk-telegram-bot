import {QueryResult} from "pg";
import {BaseRepository} from "../../infrastructure/baseRepository";

export default class TripRepository extends BaseRepository {
  public async findTrip(tripId: number): Promise<QueryResult> {
    const query = {
      name: 'find-trip-byId',
      text: `SELECT * FROM trips WHERE "Id" = $1`,
      values: [tripId],
    }

    return this.executeQuery(query);
  }
}
