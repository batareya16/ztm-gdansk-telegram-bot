import {QueryResult} from "pg";
import {BaseRepository} from "../../infrastructure/baseRepository";

export class StopRepository extends BaseRepository {
  public async findStopsByName(name: string): Promise<QueryResult> {
    const query = {
      name: 'find-stop-byname',
      text: 'SELECT * FROM stops WHERE "Name" = $1',
      values: [name],
    }

    return this.executeQuery(query);
  }

  public async findStopsById(id: number): Promise<QueryResult> {
    const query = {
      name: 'find-stop-byid',
      text: 'SELECT * FROM stops WHERE "Id" = $1',
      values: [id],
    }

    return this.executeQuery(query);
  }

  public async findStopsByIds(ids: number[]): Promise<QueryResult> {
    const query = {
      name: 'find-stops-byids',
      text: 'SELECT * FROM stops WHERE "Id" = ANY($1::int[])',
      values: [ids],
    }

    return this.executeQuery(query);
  }

  public async findStopsWithSimilarName(name: string): Promise<QueryResult> {
    const query = {
      name: 'find-stopswithsimilarname-byname',
      text: 'select "Id", "Name", similarity("Name", $1) as simil\n' +
        'from stops where "Name" is not null\n' +
        'order by simil desc\n' +
        'fetch first 5 rows only;',
      values: [name],
    }

    return this.executeQuery(query);
  }

  public async findNearestStops(latitude: number, longitude: number): Promise<QueryResult> {
    const query = {
      name: 'find-neareststops-bycoordinates',
      text: 'SELECT "Name", "Id",' +
        'min(' +
          '6371 * acos(cos(radians($1)) * cos(radians("Latitude")) * cos(radians("Longtitude") -' +
          'radians($2)) + sin(radians($1)) * sin(radians("Latitude" )))' +
        ') AS distance\n' +
        'FROM public.stops\n' +
        'WHERE "Name" is not null\n' +
        'ORDER BY "distance" fetch first 5 rows only;',
      values: [latitude, longitude],
    }

    return this.executeQuery(query);
  }
}
