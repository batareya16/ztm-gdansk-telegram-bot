import {BaseRepository} from "../../infrastructure/baseRepository";
import {QueryResult} from "pg";

export default class StopTimeRepository extends BaseRepository {
  public async findNearestStopTimes(stopId: number): Promise<QueryResult> {
    const query = {
      name: 'find-neareststoptimes-bystopid',
      text: `SELECT stopstime.* FROM stopstime
        INNER JOIN trips ON stopstime."TripId" = trips."Id"
        WHERE
        "ValidDate" = $1 and
        "ArrivalTime" > $2 and
        "StopId" = $3
        ORDER BY "ArrivalTime"
        fetch first 7 rows only`,
      values: [
        new Date().toISOString().slice(0, 10),
        new Date().toLocaleString(),
        stopId],
    }

    return this.executeQuery(query);
  }

  public async findTripStopTimes(tripId: number): Promise<QueryResult> {
    const query = {
      name: 'find-tripstoptimes-bytripid',
      text: `SELECT stopstime.* FROM stopstime
         INNER JOIN trips ON stopstime."TripId" = trips."Id"
          WHERE
          "ValidDate" = $1 and
          "TripId" = $2`,
      values: [
        new Date().toISOString().slice(0, 10),
        tripId],
    }

    return this.executeQuery(query);
  }
}
