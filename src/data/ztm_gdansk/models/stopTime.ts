export default class StopTime {
  ValidDate!: Date;
  RouteId!: number;
  TripId!: number;
  StopId!: number;
  ArrivalTime!: Date;
  DepartureTime!: Date;
}
