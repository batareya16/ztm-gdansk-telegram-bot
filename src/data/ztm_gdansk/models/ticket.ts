export default class Ticket {
  Number!: string;
  Description!: string;
  Zone!: string;
  ValidTo!: Date;
}
