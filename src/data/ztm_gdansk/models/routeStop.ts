export default class RouteStop {
  ValidDate!: Date;
  StopName!: string;
  StopId!: number;
  Order!: number;
  TripId!: number;
}
