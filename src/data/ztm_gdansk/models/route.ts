export default class Route {
  Id!: number;
  ValidDate!: Date;
  ShortName!: string;
  LongName!: string;
  Type!: string;
}
