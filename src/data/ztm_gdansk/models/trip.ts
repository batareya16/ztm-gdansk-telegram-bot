export default class Trip {
  ValidDate!: Date;
  Code!: string;
  RouteId!: number;
  TripId!: number;
  ShortName!: string;
  Direction!: number;
  RouteType!: string;
  Headsign!: string;
}
