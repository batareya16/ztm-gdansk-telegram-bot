export default class Stop {
  Id!: number;
  Code!: string;
  Name!: string;
  Description!: string;
  ValidDate!: Date;
}
