import {QueryResult} from "pg";
import {BaseRepository} from "../../infrastructure/baseRepository";

export class StopRepository extends BaseRepository {
  public async findStopsByName(name: string): Promise<QueryResult> {
    const query = {
      name: 'find-stop-byname',
      text: 'SELECT * FROM stops WHERE "Name" = $1 and "ValidDate" = $2',
      values: [name, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }

  public async findStopsById(id: number): Promise<QueryResult> {
    const query = {
      name: 'find-stop-byid',
      text: 'SELECT * FROM stops WHERE "Id" = $1 and "ValidDate" = $2',
      values: [id, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }

  public async findStopsWithSameNameById(id: number): Promise<QueryResult> {
    const query = {
      name: 'find-stopswithsamename-byid',
      text: 'SELECT * FROM stops WHERE "Name" = (SELECT "Name" FROM stops WHERE "Id" = $1 and "ValidDate" = $2) and "ValidDate" = $2',
      values: [id, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }

  public async findStopsWithSimilarName(name: string): Promise<QueryResult> {
    const query = {
      name: 'find-stopswithsimilarname-byname',
      text: 'select distinct("Name"), min("Id") as "Id", similarity("Name", $1) as simil\n' +
        'from stops where "Name" is not null and "ValidDate" = $2\n' +
        'group by "Name"\n' +
        'order by simil desc\n' +
        'fetch first 5 rows only;',
      values: [name, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }

  public async findNearestStops(latitude: number, longitude: number): Promise<QueryResult> {
    const query = {
      name: 'find-neareststops-bycoordinates',
      text: 'SELECT distinct("Name"), min("Id") as "Id",' +
        'min(' +
          '6371 * acos(cos(radians($1)) * cos(radians("Latitude")) * cos(radians("Longtitude") -' +
          'radians($2)) + sin(radians($1)) * sin(radians("Latitude" )))' +
        ') AS distance\n' +
        'FROM public.stops\n' +
        'WHERE "Name" is not null and "ValidDate" = $3\n' +
        'group by "Name"\n' +
        'ORDER BY "distance" fetch first 5 rows only;',
      values: [latitude, longitude, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }
}
