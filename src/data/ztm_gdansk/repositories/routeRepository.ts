import {QueryResult} from "pg";
import {BaseRepository} from "../../infrastructure/baseRepository";

export class RouteRepository extends BaseRepository {
  public async findRoutesByIds(ids: number[]): Promise<QueryResult> {
    const query = {
      name: 'find-routes-byids',
      text: `SELECT * FROM routes WHERE "ValidDate" = $1 and "Id" in (${ids.map((x, i) => `$${(i+2)}`).join(",")})`,
      values: [new Date().toISOString().slice(0, 10), ...ids],
    }

    return this.executeQuery(query);
  }

  public async findRoutesByShortName(name: string): Promise<QueryResult> {
    const query = {
      name: 'find-routes-byshortname',
      text: `SELECT * FROM routes WHERE "ValidDate" = $1 and "ShortName" ilike $2`,
      values: [new Date().toISOString().slice(0, 10), name],
    }

    return this.executeQuery(query);
  }
}
