import {BaseRepository} from "../../infrastructure/baseRepository";
import {QueryResult} from "pg";

export default class RouteStopRepository extends BaseRepository {
  public async findTripsStops(routeId: number, tripIds: number[]): Promise<QueryResult> {
    const query = {
      name: 'find-routestops-bytripids',
      text: `SELECT
        tripstops."ValidDate", "RouteId", "TripId", stops."Name" as "StopName", "StopId", "Order"
        FROM tripstops
        INNER JOIN stops on tripstops."StopId" = stops."Id" and tripstops."ValidDate" = stops."ValidDate"
        WHERE tripstops."ValidDate" = $1 and "RouteId" = $2 and "TripId" in (${tripIds.map((x, i) => `$${(i+3)}`).join(",")})
        ORDER BY "TripId", "Order"`,
      values: [new Date().toISOString().slice(0, 10), routeId, ...tripIds],
    }

    return this.executeQuery(query);
  }
}
