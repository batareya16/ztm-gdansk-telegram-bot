import {QueryResult} from "pg";
import {BaseRepository} from "../../infrastructure/baseRepository";

export default class TripRepository extends BaseRepository {
  public async findValidDirectedRouteTrips(routeId: number, direction: number): Promise<QueryResult> {
    const query = {
      name: 'find-validdirectedtrips-byrouteid',
      text: `SELECT * FROM trips WHERE
        "RouteId" = $1 and
        "ValidDate" = $2 and
        "Direction" = $3 and
        "RouteType" in ('MAIN', 'SIDE')`,
      values: [routeId, new Date().toISOString().slice(0, 10), direction],
    }

    return this.executeQuery(query);
  }

  public async findValidMainRouteTrip(routeId: number): Promise<QueryResult> {
    const query = {
      name: 'find-validdirectedmaintrip-byrouteid',
      text: `SELECT * FROM trips WHERE
        "RouteId" = $1 and
        "ValidDate" = $2 and
        "Direction" = 1 and
        "RouteType" = 'MAIN'`,
      values: [routeId, new Date().toISOString().slice(0, 10)],
    }

    return this.executeQuery(query);
  }
}
