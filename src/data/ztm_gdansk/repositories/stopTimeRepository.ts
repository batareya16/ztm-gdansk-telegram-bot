import {BaseRepository} from "../../infrastructure/baseRepository";
import {QueryResult} from "pg";
import {addDays} from "../../../commands/infrastructure/dateHelper";

export default class StopTimeRepository extends BaseRepository {
  public async findStopTimeByTrips(stopId: number, routeId: number, tripIds: number[]): Promise<QueryResult> {
    const query = {
      name: 'find-stoptime-bytripsandstopid',
      text: `SELECT * FROM stopstime WHERE
        ("ValidDate" = $1 or "ValidDate" = $2) and
        "StopId" = $3 and
        "RouteId" = $4 and
        "TripId" in (${tripIds.map((x, i) => `$${(i+5)}`).join(",")})
        ORDER BY "ArrivalTime"`,
      values: [
        new Date().toISOString().slice(0, 10),
        addDays(new Date(), 1).toISOString().slice(0, 10),
        stopId,
        routeId,
        ...tripIds],
    }

    return this.executeQuery(query);
  }
}
