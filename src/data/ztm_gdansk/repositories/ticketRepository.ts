import { QueryResult } from "pg";
import { BaseRepository } from "../../infrastructure/baseRepository";

export default class TicketRepository extends BaseRepository {
  public async findTicketsByNumber(number: string): Promise<QueryResult> {
    const query = {
      name: 'find-tickets-bynumber',
      text: `SELECT * FROM tickets WHERE "Number" = $1`,
      values: [number],
    }

    return this.executeQuery(query);
  }
}
