import TeleBot from 'telebot'
import { config } from 'dotenv'
import BotWrapper from "./infrastructure/BotWrapper";
import {CommandProcessorsRegistry} from "./infrastructure/commands/registry";
import StringHelper from "./commands/infrastructure/stringHelper";
import UserActionHelper from "./infrastructure/helpers/userActionHelper";
config()

// initialization methods
StringHelper.initialize();

const bot = new TeleBot(process.env.TELEGRAM_TOKEN as string);
const userActionService = new UserActionHelper();

async function processRequest(botWrapper: BotWrapper, msg: BotRequest, processorType: string) {
  const commandProcessors = CommandProcessorsRegistry.getProcessors();
  for (const procType of commandProcessors) {
    const processor = new procType();
    if (processor.getProcessorType() == processorType) {
      await processor.processCommand(botWrapper, msg);
    }
  }
}

bot.on(['/admin'], async (msg: BotRequest) => {
  if (msg.from.id !== 100968136) {
    return;
  }

  const botWrapper = new BotWrapper(bot, msg);
  await processRequest(botWrapper, msg, "text");
})

const validUserSpecialCommands = ['/start', '/help', '/allbots'];
bot.on(['text', 'callbackQuery', 'channelPost', ...validUserSpecialCommands], async (msg: BotRequest) => {
  if (msg.text?.startsWith("/") && !validUserSpecialCommands.includes(msg.text)) return;
  await userActionService.validateUserAction(msg);
  const botWrapper = new BotWrapper(bot, msg);
  await processRequest(botWrapper, msg, "text");
});

bot.on('location', async (msg: any) => {
  const botWrapper = new BotWrapper(bot, msg);
  await processRequest(botWrapper, msg, "location");
})

// Sticker handler
bot.on('sticker', async (msg: any) => {
  await msg.reply.text('🚌');
})

bot.start()
